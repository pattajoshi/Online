package BrowserInitialization;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class MainClass {
	
	public static  WebDriver driver=null;
	static String Path="C:\\Users\\ipattaj\\Desktop\\Selenium-Automation\\chromedriver.exe";
	

	public static WebDriver InitChromeDriver(){
		
		System.setProperty("webdriver.chrome.driver" ,Path);
	    driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://phptravels.org/clientarea.php");
		
       return driver;
		
	}
	
	
}