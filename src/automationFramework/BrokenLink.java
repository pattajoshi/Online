package automationFramework;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindAll;

public class BrokenLink {
	
	public static List FindAllLinks(WebDriver driver){
		
		List<WebElement> elementList=new ArrayList();
		 
		elementList=driver.findElements(By.tagName("a"));
		elementList.addAll(driver.findElements(By.tagName("image")));
		List FinalList=new ArrayList();
		
		for(WebElement element : elementList){
			
			if(element.getAttribute("href")!=null){
				FinalList.add(element);
				
			}
		}
		return FinalList;
				
	}
	
	public static String LinkBroken(URL url) throws IOException {
		
		String responce="";
		
		HttpURLConnection connection= (HttpURLConnection) url.openConnection();
		
		connection.connect();
		responce=connection.getResponseMessage();
		connection.disconnect();
			
		return responce;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FirefoxDriver driver=new FirefoxDriver();
		driver.get("http://toolsqa.wpengine.com/automation-practice-switch-windows/");
		List<WebElement> allImage=FindAllLinks(driver);
		
		int size=allImage.size();
		System.out.println(size);

	}

	
	

}
