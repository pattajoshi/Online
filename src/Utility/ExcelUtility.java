package Utility;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtility {
	
	private static XSSFWorkbook ExcelWorkbook;
	private static XSSFSheet ExcelSheet;
	private static XSSFCell Cell;
	private static XSSFRow Row;
	
	

public static void SetExcelFile (String Path, String Sheetname) throws IOException

{
		try {
		FileInputStream ExcelFile =new FileInputStream(Path);
	     ExcelWorkbook =new XSSFWorkbook(ExcelFile);
	     ExcelSheet = ExcelWorkbook.getSheet(Sheetname);
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		throw(e);
	}
	
}

public static String getCelldata(int RowNum, int CellNum)
{
	
	Cell=ExcelSheet.getRow(1).getCell(1);
	String CellData= Cell.getStringCellValue();
	return CellData;
	
}

public static String setCelldata(String Result, int RowNum, int ColNum)throws IOException{
	
	try{
		
		Row= ExcelSheet.getRow(RowNum);
		Cell= Row.getCell(ColNum);
		
		if(Cell==null) {
			
			Cell=Row.createCell(ColNum);
			Cell.setCellValue(Result);
		}
		
		else{
			
			Cell.setCellValue(Result);
				
		}
	
	
	FileOutputStream fileout= new FileOutputStream(Constant.Path + Constant.TestData);
	
	ExcelWorkbook.write(fileout);
	fileout.close();
	
	}catch(Exception e){
		throw(e);
	}
	return Result;
	
}

	}
	


