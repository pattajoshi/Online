package com.online.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.seleniumhq.jetty7.util.log.Log;
import org.testng.Assert;

import com.gargoylesoftware.htmlunit.javascript.host.file.File;
import com.sun.jna.platform.FileUtils;

import BrowserInitialization.MainClass;

public class Loginpage  {
	
	public static final WebElement EMAIL = MainClass.driver.findElement(By.xpath("//input[@id='inputEmail']"));
    public static final WebElement PASSWORD=MainClass.driver.findElement(By.xpath("//input[@id='inputPassword']"));
    public static final WebElement LOGIN_BUTTON=MainClass.driver.findElement(By.xpath("//div/input[@id='login']"));
	private static final int OutputType = 0;
	private static final int FILE = 0;

	public static void LogIn(){	 
		
		Log.info("Email element is visible");
		Assert.assertTrue(EMAIL.isDisplayed());
		Log.info("password element is visible");
		Assert.assertTrue(PASSWORD.isDisplayed());
			
    	EMAIL.click();
    	 MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    	EMAIL.sendKeys("ipsita2015.pattajoshi@gmail.com");
    	 MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    	PASSWORD.click();
    	 MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    	PASSWORD.sendKeys("Selenium123");
    	 MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    	LOGIN_BUTTON.click();
    	 MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    	 
    	
    	 	
	}
       
 
}
