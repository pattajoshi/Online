package com.online.page;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.seleniumhq.jetty7.util.log.Log;

import BrowserInitialization.MainClass;

public class LogOutpage {
	
	public static final WebElement LOGIN_USER=MainClass.driver.findElement(By.xpath( "//div[@id='bs-example-navbar-collapse-1']/ul[2]/li/a[@class='dropdown-toggle']"));
	public static final WebElement LOGOUT=MainClass.driver.findElement(By.xpath("//*[@id='Secondary_Navbar-Account-Logout']/a"));
    public static final WebElement LOGOUTPAGE=MainClass.driver.findElement(By.xpath("//section[@id='main-body']/div[1]/div/div/div[2]"));
	public static void Logout(){
		
		Log.info("User log in button is visible");
		Assert.assertTrue(LOGIN_USER.isDisplayed());
	    MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		LOGIN_USER.click();
		 MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		Assert.assertTrue(LOGOUT.isDisplayed());
		 MainClass.driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		LOGOUT.click();
		
	}
	
}
